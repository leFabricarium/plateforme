# Arduino

Initiation à l'utilisation des cartes Arduino, présenté au Fabricarium par Axel CHEMIN :) .

![plusieurs cartes Arduino](Arduino/P1010274_recadre.JPG)

## A propos

Pourquoi Arduino ? C’est TROP BIEN Arduino ! En fait, pendant longtemps, pour faire de l’électronique, même en tant que débutant, il fallait forcément des connaissances de dingues et beaucoup de matos. Puis Arduino est arrivé.

Arduino c’est, pour faire simple, une carte électronique programmable. Ainsi vous pouvez mettre du code à l’intérieur et utiliser ses nombreuses entrées/sorties sur lesquels vous pouvez mettre d’autres composant. Et ainsi c’est presque fait, ajoutez des leds => une horloge, ajoutez un haut parleur => une alarme, ajoutez deux moteurs => un véhicule, mettez-y un capteur de température => une station météo : les possibilités sont infinies et pleins d'autres idées sur <https://www.hackster.io/arduino> !

## Pré-requis

Il est préférable d’avoir les bases en électronique pour cette première formation, mais le formateur adaptera son discours du plus simple, au plus intéressé !


## Présentation

La carte Arduino est composé de :
* un microcontrolleur (le cerveau)


![schéma des pins de la Uno](Arduino/pinout.png)


